package com.demo;

/**
 * 输入的线程，给变量复制
 * 交替赋值
 */
public class Input implements Runnable {
    private Resource r;

    public Input(Resource r){
        this.r = r;
    }

    @Override
    public void run(){
        int i = 0;
        while(true){
            synchronized(r){
                if(r.flag) {
                    try {
                        r.wait();
                    } catch (Exception ex) {
                    }
                }

                if(i%2 == 0){
                    r.name = "张三";
                    r.sex = "女";
                }else{
                    r.name = "lisi";
                    r.sex = "man";
                }
                r.flag = true;
                r.notify();
            }
            i++;
        }
    }
}
